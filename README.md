README
======

*ipl* is the __icebrg.us Package Loader__ system.

Requirements
============

* A decent *nix operating system (has /bin/sh)
* The programs used in the command string of the alias you intend to use (ex.
  for bb-hg, hg, for github, git...)

Usage
=====

    <?php
    require 'ipl.php';

    require ipl('bb-hg/icebrg/lib/1.0');

    require 'icebrg/lib/autoloader.php';

PEAR works just fine for me - why this?
=======================================

PEAR doesn't work "just fine for me," notably because a lot of important
applications don't have PEAR channels. PEAR simply wasn't designed for the
development style PHP developers have come to use. PEAR2, while being better
designed, has very few packages. I needed a simple solution that would allow me
to load PHP libraries from remote repositories with minimal fuss. *ipl* fits my
development workflow - I never said it had to fit yours.

Note on using packages from private repositories
=================================================

Depending on your OS, you may see a GUI or CLI prompt for an OpenSSH username
and password. If you enter the username and password for the service you are
cloning from, everything should be fine. Loading packages from public
repositories should not show any prompt.