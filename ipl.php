<?php
/**
 * Defines the icebrg.us Package Loader functions.
 * @package   ipl
 * @author    Jonathan Chan <jchan@icebrg.us>
 * @copyright 2011 Jonathan Chan <jchan@icebrg.us>
 * @license   http://www.opensource.org/licenses/bsd-license.php The BSD 2-Clause License
 */

define('IPL_VERSION', 0.1);

if ( ! defined('IPL_ALIASES')) {
    if (ini_get('ipl_aliases')) {
        define('IPL_ALIASES', ini_get('ipl_aliases'));
    } else {
        define('IPL_ALIASES', 'bb-hg=hg clone -r %3$s https://bitbucket.org/%1$s/%2$s %4$s,'
                            . 'bb-git=git clone https://bitbucket.org/%1$s/%2$s.git %4$s && cd %4$s && git checkout %3$s,'
                            . 'github=git clone https://github.com/%1$s/%2$s.git %4$s && cd %4$s && git checkout %3$s');
    }
}

if ( ! defined('IPL_SAVEDIR')) {
    if (ini_get('ipl_savedir')) {
        define('IPL_SAVEDIR', ini_get('ipl_savedir'));
    } else {
        define('IPL_SAVEDIR', 'lib/');
    }
}

/**
 * Loads a library.
 * @param string $name The name of the library to load, formatted as
 * <pre>alias/vendor/package/version</pre>. The vendor, package and version will
 * be inserted into the alias's command string, as specified in the $aliases
 * argument.
 * @param bool $addToPath Whether or not to add the directory of the loaded
 * library to PHP's include path. Defaults to false. Useful for libraries whose
 * repositories store classes in a sub-tree. Only applies if the library has not
 * been previously loaded.
 * @param string $saveDir The directory to save the library in. Defaults to the
 * IPL_SAVEDIR constant.
 * @param string $aliases A string of aliases and command strings, which, when
 * run, should load a library. Defaults to the IPL_ALIASES constant. The format
 * is name1=some command,name2=another command. The command strings are passed
 * as format specifiers to the sprintf function, and are given four arguments:
 * the vendor name, the package name, the version, and the directory to save the
 * library to.
 * @return string The local directory in which the library is stored.
 * @throws InvalidArgumentException|RuntimeException
 */
function ipl($name, $addToPath = false, $saveDir = IPL_SAVEDIR, $aliases = IPL_ALIASES)
{
    static $loaded = array();
    static $parsedAliasesRaw = null;
    static $parsedAliases = null;

    // workaround to cache a static variable that is the result of a function call
    if ($parsedAliasesRaw != $aliases) {
        $parsedAliases = ipl_parse_aliases($aliases);
        $parsedAliasesRaw = $aliases;
    }

    // if this package has already been loaded, return the cached saveDir

    if (isset($loaded[$name])) {
        return $loaded[$name];
    }

    // parse name

    $nameParts = explode('/', $name);

    if (count($nameParts) != 4) {
        throw new InvalidArgumentException("\$name must contain an alias, vendor, package name, and version, separated by /s.");
    }

    list($alias, $vendor, $package, $version) = $nameParts;

    // create the saveDir if it doesn't exist

    $saveDir = rtrim($saveDir, '/') . '/';

    if ( ! is_dir($saveDir)) {
        if ( ! mkdir($saveDir)) {
            throw new RuntimeException("Failed to create save directory at " . $saveDir . ".");
        }
    }

    // check if already downloaded

    $savePath = $saveDir . $vendor . '/' . $package . '/';

    if ( ! is_dir($savePath)) {
        // create command

        if ( ! isset($parsedAliases[$alias])) {
            throw new InvalidArgumentException("There is no registered alias '$alias'.");
        }

        if ( ! is_dir($saveDir . $vendor)) {
            if ( ! mkdir($saveDir . $vendor)) {
                throw new RuntimeException("Failed to create vendor directory at {$saveDir}{$vendor}.");
            }
        }

        $vendor     =   escapeshellarg($vendor);
        $package    =   escapeshellarg($package);
        $version    =   escapeshellarg($version);
        $savePath   =   escapeshellarg($savePath);

        $command = '/bin/sh -c "' . sprintf($parsedAliases[$alias], $vendor, $package, $version, $savePath) . '"';

        // execute the command - have to do all this proc_open business to
        // suppress output...

        $descriptors = array(
            0   =>  array('pipe', 'r'), // STDIN
            1   =>  array('pipe', 'w'), // STDOUT
            2   =>  array('pipe', 'a') // STDERR
        );

        $process = proc_open($command, $descriptors, $pipes);

        // I am not sure why this is necessary. Discovered during testing that
        // without it, I'd get a return code of 128.
        foreach ($pipes as $pipe) {
            fgets($pipe);
        }

        $status = proc_close($process);

        if ($status != 0) {
            throw new RuntimeException("Failed to execute install command '$command'.");
        }
    }

    // add to the include path
    set_include_path(get_include_path() . PATH_SEPARATOR . $savePath);

    $loaded[$name] = $saveDir;

    return $savePath;
}

/**
 * Parses IPL alias lists.
 * @param string $aliases A list of aliases to parse.
 * @return array An array of aliases, represented as arrays containing name and
 * command strings.
 * @throws RuntimeException if the IPL_ALIASES constant is undefined or empty.
 */
function ipl_parse_aliases($aliases)
{
    if (empty($aliases)) {
        throw new RuntimeException("\$aliases must not be empty.");
    }

    $specs = array();

    foreach (explode(',', $aliases) as $spec) {
        if ( ! empty($spec)) {
            $parts = explode('=', $spec);

            if (count($parts) != 2) {
                throw new InvalidArgumentException("Each specification must be formatted as key=value.");
            }

            $specs[$parts[0]] = $parts[1];
        }
    }

    return $specs;
}

/* End of File ipl.php */