<?php
require '../ipl.php';

// This is for the purposes for the example - typically you'd be require()ing a
// PHP file instead.
$license = file_get_contents(ipl('github/sebastianbergmann/phpunit/3.6.5') . 'LICENSE');

echo $license;
