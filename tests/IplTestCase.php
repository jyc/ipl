<?php
/**
 * @package somepackage
 * @author Jonathan Chan <jchan@icebrg.us>
 * @copyright 2011 Jonathan Chan <jchan@icebrg.us>
 * @license http://www.opensource.org/licenses/bsd-license.php The BSD 2-Clause License
 */
namespace jonathanyc\ipl;

require __DIR__ . '/../ipl.php';

/**
 *
 * @author Jonathan Chan <jchan@icebrg.us>
 * @copyright 2011 Jonathan Chan <jchan@icebrg.us>
 */
class IplTestCase extends \PHPUnit_Framework_TestCase
{
    public function testIplParseAliases()
    {
        // Validation tests

        try {
            ipl_parse_aliases('');

            $this->fail("Should throw RuntimeException if the aliases are empty.");
        } catch (\RuntimeException $e) { }

        try {
            ipl_parse_aliases('malformed,something=value');

            $this->fail("Should throw InvalidArgumentException when a specification lacks a value.");
        } catch (\InvalidArgumentException $e) { }

        // Functionality tests

        $this->assertEquals(array(
                                 'foo'  =>  'some command',
                                 'bar'  =>  'other command'
                            ), ipl_parse_aliases('foo=some command,bar=other command'));
    }

    public function testIpl()
    {
        // Validation tests

        try {
            ipl('needs/four/parts');

            $this->fail("Should throw InvalidArgumentException if the name lacks the proper parts.");
        } catch (\InvalidArgumentException $e) { }

        try {
            ipl('undefined-alias/vendor/package/version');

            $this->fail("Should throw InvalidArgumentException if the supplied alias is not registered.");
        } catch (\InvalidArgumentException $e) { }

        ipl('write/test/test/test', false, __DIR__ . '/data/', 'write=mkdir -p %4$s');

        $this->assertTrue(is_dir(__DIR__ . '/data/test/test'), "Should create directory test/test/test.");

        // Functionality tests

        $result = require ipl('alias/vendor/package/1.0', false, __DIR__ . '/data/', 'alias=') . 'file.php';
        $this->assertEquals(require __DIR__ . '/data/vendor/package/file.php', $result, "Should require data/vendor/package/file.php.");

        // Clean up

        exec('rm -rf ' . IPL_SAVEDIR);
        exec('rm -rf ' . __DIR__ . '/data/test/');
    }
}

/* End of File IplTestCase.php */